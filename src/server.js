import express from "express";
import userRouter from "./controllers/user-controller.js";

const app = express();

app.use(userRouter);

app.get("/", (req, res) => {
    res.send('Hello');
})
export default app;
